<?php


use App\Queri;
use App\Feedback;
use App\SiteSettings;
use App\Product;
// use Alert;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.welcome');
// })->name('home');
Route::get('',['as' => 'home_frontend', 'uses' => 'FrontendController@showHome']);
Route::post('add-query',['as' => 'add_query', 'uses' => 'FrontendController@addQuery']);
Route::post('add-feedback',['as' => 'add_feedback', 'uses' => 'FrontendController@addFeedback']);
Route::get('products-page',function(){
	$products = Product::paginate(4);
	return view('frontend.product', compact('products'));
})->name('products_page');






//admin routes

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function() {
	Route::get('admin',['as' => 'admin_home','uses' => 'AdminController@home']);
	Route::get('query-page',['as' => 'query_page', 'uses' => 'AdminController@showQueries']);
	Route::get('delete-query/{id}', function($id){
		Queri::where('id','=',$id)->delete();
		return Redirect::route('query_page');
	})->name('delete_query');
	Route::get('update-read/{id}', function($id){
		Queri::where('id','=',$id)->update(['read_flag' => 1]);
		return Redirect::route('query_page');
	})->name('update_read');
	Route::get('feedback-page',['as' => 'feedback_page', 'uses' => 'AdminController@showFeedback']);
	Route::get('mark-active/{id}',function($id){
		Feedback::where('id','=',$id)->update(['active_flag' => 1]);
		return Redirect::route('feedback_page');
	})->name('mark_active');
	Route::get('delete-feedback/{id}', function($id){
		Feedback::where('id','=',$id)->delete();
		return Redirect::route('feedback_page');
	})->name('delete_feedback');
	Route::post('add-banner',['as' => 'add_banner', 'uses' => 'AdminController@addBanner']);
	Route::get('show-settings',['as' => 'show_settings', 'uses' => 'AdminController@showSettings']);
	Route::get('hide-feedback', function(){
		SiteSettings::where('set_name','=','Feedback')->update(['active' => 0]);
		return Redirect::route('show_settings');
	})->name('hide_feedback');
	Route::get('show-feedback', function(){
		SiteSettings::where('set_name','=','Feedback')->update(['active' => 1]);
		return Redirect::route('show_settings');
	})->name('show_feedback');
	Route::post('update-contact', function(){
		SiteSettings::where('set_name','=','Contact')->update(['value' => Input::get('contact')]);
		Alert::success(' ','Contact Updated');
		return Redirect::route('show_settings');
	})->name('update_contact');
	Route::post('update-mail', function(){
		SiteSettings::where('set_name','=','Email')->update(['value' => Input::get('mail')]);
		Alert::success(' ','Email Updated');
		return Redirect::route('show_settings');
	})->name('update_mail');
	Route::post('update-address', function(){
		SiteSettings::where('set_name','=','Address')->update(['value' => Input::get('address')]);
		Alert::success(' ','Address Updated');
		return Redirect::route('show_settings');
	})->name('update_address');
	Route::get('products',function(){
		return view('admin.product');
	})->name('products');
	Route::get('show-product',function(){
		return view('admin.product');
	})->name('show_product');
	Route::post('add-product',['as' => 'add_product','uses' => 'AdminController@addProduct']);
	Route::post('search-product',['as' => 'search_product', 'uses' => 'AdminController@searchProduct']);
	Route::post('update-product',['as' => 'update_product', 'uses' => 'AdminController@updateProduct']);
	Route::get('deleteproduct/{id}', function($id){
		Product::where('id','=',$id)->delete();
		Alert::success(' ','Product Deleted');
		return Redirect::route('show_product');
	})->name('deleteproduct');
	Route::get('account-setting',['as' => 'account_setting', 'uses' => 'AdminController@account']);
	Route::post('changePassword',['as' => 'changePassword','uses' => 'HomeController@changePassword']);
	Route::post('update-details',['as' => 'update_details', 'uses' => 'AdminController@updateDetails']);
	Route::get('visitors',['as' => 'visitors', 'uses' => 'AdminController@userlist']);
	Route::get('owner-info',['as' => 'owner_info','uses' => 'AdminController@showownerpage']);
	Route::post('update-owner',['as' => 'update_owner','uses' => 'AdminController@updateOwner']);
	Route::post('upload-ownerpic',['as' => 'upload_ownerpic','uses' => 'AdminController@uploadOwnerPic']);
	Route::post('add-owner',['as' => 'add_owner','uses' => 'AdminController@addOwner']);
});


Route::get('productimage',function(){
	return view('admin.product');
})->name('productimage');


