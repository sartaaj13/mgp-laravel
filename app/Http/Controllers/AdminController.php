<?php

namespace App\Http\Controllers;
use App\Visitor;
use App\Queri;
use App\BannerSetting;
use App\SiteSettings;
use App\Product;
use App\User;
use App\Feedback;
use App\Owner;
use Alert;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;
use Exception;

use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function home()
    {

        $feed = Feedback::where('rating','=',5)->get();
        $five = count($feed);
        $feed = Feedback::where('rating','=',4)->get();
        $four = count($feed);
        $feed = Feedback::where('rating','=',3)->get();
        $three = count($feed);
        $feed = Feedback::where('rating','=',2)->get();
        $two = count($feed);
        $feed = Feedback::where('rating','=',1)->get();
        $one = count($feed);
        $feed = Queri::get();
        $totalqueries = count($feed);
        $feed = Queri::where('read_flag','=',1)->get();
        $readqueries = count($feed);
        $feed = Visitor::get();
        $user = count($feed);
        $feed = Product::get();
        $product = count($feed);
        $name = Auth::user();
        Alert::message('Welcome '.$name->name, 'Hey Admin!');
        return view('admin.dashboard',compact('one','two','three','four','five','totalqueries','readqueries','user','product'));
    }
    public function showQueries()
    {
    	$visitor = Visitor::get();
    	foreach ($visitor as $v) {
    	 	$query = Queri::where('vid','=',$v->id)->get();
    	 	if(count($query)==0)
    	 	{
    	 		$v->query = null;
    	 	}
    	 	else{
    	 		$v->query = $query;
    	 	}
    	}
    	return view('admin.queries',compact('visitor'));
    }
    public function showFeedback()
    {
    	$feedback = DB::table('feedback')
    				  ->join('visitors','feedback.vid','=','visitors.id')
    				  ->select('feedback.*','visitors.name','visitors.email','visitors.contact')
    				  ->get();
    	return view('admin.feedback',compact('feedback'));
    }
    public function showSettings()
    {
        $url = BannerSetting::where('name','=','Banner')->pluck('url');
        $feedback = SiteSettings::where('set_name','=','Feedback')->pluck('active');
        $contact = SiteSettings::where('set_name','=','Contact')->pluck('value');
        $mail = SiteSettings::where('set_name','=','Email')->pluck('value');
        $address = SiteSettings::where('set_name','=','Address')->pluck('value');
        // return $url;
        return view('admin.settings', compact('url','feedback','contact','mail','address'));
    }
    public function addBanner(Request $request)
    {
    	try{
            $now = Carbon::now();
	    	$file = Input::file('file');
	    	$ext = $file->getClientOriginalExtension();
	    	$name = "Banner";
	    	$filename = $name."-".$now->hour."-".$now->second.".".$ext;
	    	$file->move("banner/", $filename);
	    	$file_url = str_replace("/index.php", "", url('/'))."/banner/$filename";
            BannerSetting::where('name','=','Banner')->update(['url' => $file_url]);
	    	return Redirect::route('show_settings');
    	}catch(Exception $e)
    	{
    		Alert::error('File Should not exceed 8mb','File too Large');
    		return Redirect::route('setting');
    	}

    }
    public function addProduct()
    {
        try{
            $now = Carbon::now();
            $item_no = Input::get('item-no');
            $items = Product::where('item_no','=', $item_no)->get();
            if(count($items)==1)
            {
                Alert::error(' ','Item No Already Exists');
                return Redirect::route('show_product');
            }
            $product = new Product;
            $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $name = "item_no";
            $filename = $name."_".$now->hour."_".$now->second."_".Input::get('item-no').".".$ext;
            $file->move("products/", $filename);
            $file_url = str_replace("/index.php", "", url('/'))."/products/$filename";
            $product->item_no = Input::get('item-no');
            $product->price = Input::get('price');
            $product->url = $file_url;
            $product->tag = Input::get('tag');
            $product->save();
            Alert::success(' ','Product Uploaded');
            return Redirect::route('show_product');
        }catch(Exception $e)
        {
            Alert::error('File Should not exceed 8mb','File too Large');
            return Redirect::route('show_product');
        }
    }
    public function searchProduct()
    {
        $item = Input::get('item-no');
        $searchproduct = Product::where('item_no','=',$item)->get();
        return view('admin.product', compact('searchproduct'));
        // return Redirect::route('show_product')->with(compact('searchproduct'));
    }
    public function updateProduct()
    {
        $produc = array();
        $product['item_no'] = Input::get('item-no');
        $product['tag'] = Input::get('tag');
        $product['price'] = Input::get('price');
        if(Product::where('id','=',Input::get('id'))->update($product))
        {
            Alert::success(' ','Updated');
            return Redirect::route('show_product');
        }
        else
        {
            Alert::error(' ','Something went wrong');
            return Redirect::route('show_product');   
        }
    }
    public function account()
    {
        $user = Auth::user();
        return view('admin.account', compact('user'));
    }
    public function updateDetails()
    {
        $id = Auth::user();
        if (User::where('id','=',$id->id)->update(['name' => Input::get('name'), 'email' => Input::get('email')])) 
        {
            Alert::success(' ','Details Updated');
            return Redirect::route('account_setting');
        }
        else
        {
            Alert::error('Something Went Wrong','Oops!');
            return Redirect::route('account_setting');   
        }
    }
    public function userlist()
    {
        $user = Visitor::get();
        return view('admin.users',compact('user'));
    }
    public function showownerpage()
    {
        $owner = Owner::get();
        return view('admin.ownerinfo',compact('owner'));
    }
    public function updateOwner()
    {
        if($update = Owner::where('id','=',Input::get('id'))->update(['name' => Input::get('name'), 'desgnation' => Input::get('designation'),'message' => Input::get('message')]))
        {
            Alert::success(' ','Details Updated');
            return Redirect::route('owner_info');
        }
        else
        {
            Alert::error('Something Went Wrong','Oops!');
            return Redirect::route('owner_info');
        }
    }
    public function addOwner()
    {
        $now = Carbon::now();
        $name = Input::get('name');
        $file = Input::file('file');
        $ext = $file->getClientOriginalExtension();
        $fname = $name[0]."_".$now->second.$now->hour.".".$ext;
        $file->move("owner/",$fname);
        $file_url = str_replace("/index.php", "", url('/'))."/owner/$fname";
        $owner = new Owner;
        $owner->name = $name;
        $owner->desgnation = Input::get('designation');
        $owner->message = Input::get('message');
        $owner->url = $file_url;
        $f = $owner->save();
        if($f)
        {
            Alert::success(' ','Details Added');
            return Redirect::route('owner_info');
        }
        else
        {
            Alert::error('Something Went Wrong','Oops!');
            return Redirect::route('owner_info');
        }

    }
    public function uploadOwnerPic()
    {
        $now = Carbon::now();
        $name = Owner::where('id','=',Input::get('id'))->pluck('name');
        $file = Input::file('file');
        $ext = $file->getClientOriginalExtension();
        $fname = $name[0]."_".$now->second.$now->hour.".".$ext;
        $file->move("owner/",$fname);
        $file_url = str_replace("/index.php", "", url('/'))."/owner/$fname";
        if(Owner::where('id','=',Input::get('id'))->update(['url' => $file_url]))
        {
            Alert::success(' ','File Uploaded');
            return Redirect::route('owner_info');
        }
        else
        {
            Alert::success('Something Went Wrong','Whoops!');
            return Redirect::route('owner_info');
        }
    }
}
