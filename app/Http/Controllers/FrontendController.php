<?php

namespace App\Http\Controllers;
use App\Visitor;
use App\Queri;
use App\Feedback;
use App\BannerSetting;
use App\SiteSettings;
use App\Product;
use App\Owner;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function showHome()
    {
        $url = BannerSetting::where('name','=','Banner')->pluck('url');
        $feedback = SiteSettings::where('set_name','=','Feedback')->pluck('active');
        $contact = SiteSettings::where('set_name','=','Contact')->pluck('value');
        $con = explode('/', $contact[0]);
        $mail = SiteSettings::where('set_name','=','Email')->pluck('value');
        $email = explode('/', $mail[0]);
        $address = SiteSettings::where('set_name','=','Address')->pluck('value');
        $owner = Owner::get();
        $feedbacks = DB::table('feedback')
                      ->where('active_flag','=',1)
                      ->join('visitors','feedback.vid','=','visitors.id')
                      ->select('feedback.*','visitors.name','visitors.email','visitors.contact')
                      ->get();
        // return $con;
        $products = Product::paginate(2);
        // dd($products);
        return view('frontend.welcome', compact('url','feedback','con','email','address','feedbacks','products','owner'));
    }
    public function addVisitor($name, $email, $contact)
    {
    	$visitor = new Visitor;
    	$visitor->name = $name;
    	$visitor->email = $email;
    	$visitor->contact = $contact;
    	$visitor->save();
    	return $visitor;
    }
    public function addQuery()
    {
    	$name = Input::get('name');
    	$email = Input::get('email');
    	$contact = Input::get('contact');

    	$visitor = Visitor::FilterByName($name)->FilterByContact($contact)->get();
    	if(count($visitor)==0)
    	{
    		$v = $this->addVisitor($name, $email, $contact);
    		$query = new Queri;
    		$query->vid = $v->id;
    		$query->text = Input::get('query');
    		$query->save();
    		Alert::success('We will get back you soon','Success!!');
    		return Redirect::route('home_frontend');
    	}
    	else
    	{
    		$query = new Queri;
    		$query->vid = $visitor[0]->id;
    		$query->text = Input::get('query');
    		$query->save();
    		Alert::success('Thanks for coming back. We will reach you soon','Success!!');
    		return Redirect::route('home_frontend');
    	}
    }
    public function addFeedback()
    {
    	$name = Input::get('name');
    	$email = Input::get('email');
    	$contact = Input::get('contact');

    	$visitor = Visitor::FilterByName($name)->FilterByContact($contact)->get();
    	if(count($visitor)==0)
    	{
    		$v = $this->addVisitor($name, $email, $contact);
    		$feedback = new Feedback;
    		$feedback->vid = $v->id;
    		$feedback->rating = Input::get('rating');
    		$feedback->text = Input::get('feedback');
    		// dd($feedback);
    		$feedback->save();
    		Alert::success('For your Feedback','Thank you!!');
    		return Redirect::route('home_frontend');
    	}
    	else
    	{
    		$feed = Feedback::where('vid','=',$visitor[0]->id)->get();
    		// return count($feed);
    		if(count($feed)==0) //no previous feedback
    		{
    			$feedback = new Feedback;
	    		$feedback->vid = $visitor[0]->id;
	    		$feedback->text = Input::get('feedback');
	    		$feedback->rating = Input::get('rating');
	    		// dd($feedback);
	    		$feedback->save();
	    		Alert::success('For your Feedback','Thank you!!');
	    		return Redirect::route('home_frontend');
    		}
    		else //previos feedback
    		{
    			Feedback::where('vid','=',$visitor[0]->id)->update(['text' => Input::get('feedback'), 'rating' => Input::get('rating')]);
    			// dd($visitor);
    			Alert::success('For your Feedback','Thank you!!');
	    		return Redirect::route('home_frontend');
    		}
    	}
    }
}
