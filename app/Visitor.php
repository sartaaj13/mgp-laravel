<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    public function scopeFilterByName($query, $name)
    {
    	return $query->where('name',$name);
    }
    public function scopeFilterBYContact($query, $contact)
    {
    	return $query->where('contact', $contact);
    }
}
