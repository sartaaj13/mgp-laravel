<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuerisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('queris', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vid')->unsigned();
            $table->string('text');
            $table->integer('read_flag')->default(0);
            $table->timestamps();

            $table->foreign('vid')
                  ->references('id')->on('visitors')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queris');
    }
}
