<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item_no' => 'item',
                'price' => NULL,
                'url' => 'http://localhost:8000/products/item_no_item2.jpeg',
                'tag' => NULL,
                'created_at' => '2019-04-09 14:30:47',
                'updated_at' => '2019-04-10 05:38:31',
            ),
            1 => 
            array (
                'id' => 2,
                'item_no' => 'item 3',
                'price' => 500,
                'url' => 'http://localhost:8000/products/item_no_item 3.jpeg',
                'tag' => NULL,
                'created_at' => '2019-04-09 14:32:12',
                'updated_at' => '2019-04-09 14:32:12',
            ),
            2 => 
            array (
                'id' => 3,
                'item_no' => 'item285',
                'price' => 706,
                'url' => 'http://localhost:8000/products/item_no_item2.jpeg',
                'tag' => 'dskj',
                'created_at' => '2019-04-09 14:34:03',
                'updated_at' => '2019-04-10 05:39:44',
            ),
            3 => 
            array (
                'id' => 4,
                'item_no' => 'item2',
                'price' => 78,
                'url' => 'http://localhost:8000/products/item_no_item2.jpeg',
                'tag' => NULL,
                'created_at' => '2019-04-09 14:36:43',
                'updated_at' => '2019-04-09 14:36:43',
            ),
        ));
        
        
    }
}