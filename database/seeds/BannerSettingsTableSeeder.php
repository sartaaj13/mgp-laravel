<?php

use Illuminate\Database\Seeder;

class BannerSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banner_settings')->delete();
        
        \DB::table('banner_settings')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Banner',
                'url' => 'http://localhost:8000/banner/Banner-11-5.jpg',
                'created_at' => NULL,
                'updated_at' => '2019-04-10 11:28:05',
            ),
        ));
        
        
    }
}