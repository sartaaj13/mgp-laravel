<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'MG product',
                'email' => 'mgp@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$fYM1zqk5oiTUQ8fSP7YlVuo3e5grkOPT7wAmImANWvEFcHi8VVm6C',
                'remember_token' => NULL,
                'created_at' => '2019-04-10 06:19:07',
                'updated_at' => '2019-04-10 13:14:42',
            ),
        ));
        
        
    }
}