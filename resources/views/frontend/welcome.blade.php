<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="M.G. Product">
    <meta name="author" content="Inderjeet Singh">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/mgpicon2.jpg') }}">
    <link rel="icon" type="image/png" href="{{ asset('images/mgpicon2.jpg') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'M.G. Prodcut') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/customcss.css') }}" rel="stylesheet">
    <style>
        html,body{font-family: 'Nunito', sans-serif;}
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
        .rating{
          float: left;
          font-size: 20px;
        }
        .rating > input { display: none; } 
        .rating > label:before { 
          margin: 5px;
          font-size: 20px;
          display: inline-block;
        }

        .rating > .half:before { 
          position: absolute;
        }

        .rating > label { 
          color: #979797; 
         float: right; 
        }
        .pull-left{
          float: left;
        }
        .pull-right{
          float: right;
        }
        .sub-heading{
          color: #9d9d9d;
        }
        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }  
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }

        @font-face {
            font-family: 'myFont';
            src: url('{{asset('css/fonts/Billabong.ttf')}}');
            }
    </style>

      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    @yield('custom-styles')
</head>
<body>
@include('sweet::alert')
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home_frontend') }}">M.G. Product</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#aboutus">About Us</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{route('products_page')}}">Our Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#reachus">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
          <!-- banner -->
          <div class="container-fluid m-t-n-20">
            <div class="row">
              <div class="col-md-12 banner">
                <img src="{{ $url[0] }}" class="banner-main">
              </div>
            </div>
          </div>
          <!-- banner -->

          <!-- About -->
          <div class="container-fluid bg-about" id="aboutus">
            <div class="row justify-content-center">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 m-t-20">
                    <h2>About Us </h2><br>
                    <h4>Established in the year 1999, we, M.G.PRODUCT, are a noticeable firm indulged in manufacturing and Service providing an impeccable array of Metallized Polyester Film decorative goods, banners, hangings, crape rolls, polyeaster pardas, polyeaster jhandis, paper chains  many more. Our offered goods are known in the market for fade resistant, uniform thickness, smooth & glossy finish and cost-effectiveness</h4><br>
                    <h4>Assisted by a trained and experienced team of personnel, we have been able to carry out the complete business process in the most smooth and efficient manner. Quality has been the topmost significant priority of our company,. Also, our quality inspector specifically tests the complete product assortment on several parameters to deliver faultless products at the patrons’ end. Due to our transparent business dealings, principled business policies and highly competitive price, we have become the chief choice of our patrons spread all over the nation.</h4><br>
                    <h4>M.G.PRODUCT is co-promoted by the Chairman and Managing Director Mr. MANOJ BENGANI in the year 2000 along with the Founder Chairman Late HANUMANMAL BENGANI. Under the proficient supervision of our mentor Mr. MANOJ BENGANI, we have been able to institute ourselves as the principal firm in this field. His vast knowledge and experience assist us meeting patrons precise necessities in an effectual and economical way.</h4><br><br>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- About -->

          <!--owner-->
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-12">
                <br>
                    <h3 class="text-center">A Word from Director</h3></div>
            </div>
            @foreach($owner as $o)
              <div class="row m-t-20">
                <div class="col-md-3">
                  <img src="{{ $o->url }}">
                </div>
                <div class="col-md-9">
                  <h3>{{ $o->name }}</h3>
                  <h4 class="sub-heading">{{ $o->desgnation }}</h4>
                  <p>{{ $o->message }}</p>
                </div>
              </div>
            @endforeach
            <br>
          </div>
          <!--owner-->

          <!--customer speaks-->
          @if($feedback[0]==1)
          <div class="container-fluid bg-grey" id="customerfeedback">
            <div class="row justify-content-center">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <br>
                    <h3 class="text-center">Customer Speaks</h3>
                    <div class="slideshow-container">
                      @foreach($feedbacks as $feeds)
                      <div class="mySlides">
                        <h4><i class="fas fa-quote-left"></i> {{$feeds->text}} <i class="fas fa-quote-right"></i></h4>
                        <h4>
                        @for($i=1;$i<=$feeds->rating;$i++)
                          <i class="fas fa-star bg-gold"></i>
                        @endfor
                        </h4>
                        <h5 class="author">- {{$feeds->name}}</h5>
                      </div>
                      @endforeach
                      <a class="prev" onclick="plusSlides(-1)">❮</a>
                      <a class="next" onclick="plusSlides(1)">❯</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endif
          <!--customer speaks-->

          <!--footer-->
            <div class="container-fluid bg-black footer">
              <div class="row">
                <div class="container">
                  <div class="row">
                    <div class="col-md-2 m-t-20">
                      <h3>M.G.</h3>
                      <h3>Product</h3>
                    </div>
                    <div class="col-md-2 m-t-20">
                      <h5>Explore</h5>
                      <h6><a href="{{route('products_page')}}" style="text-decoration: none; color: #fff;">Our Products</a></h6>
                      <h6 onclick="document.getElementById('id01').style.display='block'" style="cursor: pointer;">Feedback</h6>
                    </div>
                    <div class="col-md-4 m-t-20" id="reachus">
                      <h5>Visit</h5>
                      <address>
                        <i class="fas fa-map-marker-alt"></i> {{$address[0]}}<br>
                        @foreach($con as $contact)
                          <i class="fas fa-phone"></i> {{$contact}}<br>
                        @endforeach
                        @foreach($email as $mail)
                          <i class="fas fa-envelope"></i> {{$mail}}<br>
                        @endforeach
                      </address>
                    </div>
                    <div class="col-md-4 m-t-20">
                      <h5 class="text-center">Write To Us</h5>
                      {{Form::open(['route' => 'add_query','method' => 'post', 'id' => 'add_query'])}}
                      {{ csrf_field() }}
                      <div class="feedback">
                        <input type="text" name="name" id="name" class="form-control m-b-5" required="" placeholder="Name" autocomplete="off">
                        <input type="email" name="email" id="email" class="form-control m-b-5" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Email" autocomplete="off">
                        <input type="number" name="contact" id="contact" class="form-control m-b-5" required="" placeholder="Contact Number" autocomplete="off">
                        <textarea name="query" id="query" class="form-control m-b-5" placeholder="What you want to ask?" required="" autocomplete="off"></textarea>
                      </div>
                      <div class="row justify-content-center">
                        <input type="submit" name="submit" value="Submit" class="btn btn-light">
                      </div>
                      {{Form::close()}}
                    </div>
                  </div>
                  <br>
                  <div class="row text-center">
                    <div class="col-md-12">
                      <h6 class="text-center">&copy;
                        <script>
                          document.write(new Date().getFullYear())
                        </script>
                        M.G. Product
                      </h6>
                      <a href="mailto:inderjeet.singh@bvicam.in" style="text-decoration: none;color: #ffffff;">
                        <h4 class="text-center" style="font-family: myFont;">Crafted by Inderjeet Singh</h4>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <!--modal-->
              <div id="id01" class="w3-modal">
              <div class="w3-modal-content w3-animate-bottom w3-card-4">
                <header class="w3-container w3-teal"> 
                  <span onclick="document.getElementById('id01').style.display='none'" 
                  class="w3-button w3-display-topright">&times;</span>
                  <h3 class="modal-h3">Give Feedback</h3>
                </header>
                <div class="w3-container">
                  {{Form::open(['route' => 'add_feedback', 'method' => 'post', 'id' => 'add_feedback'])}}
                  {{csrf_field()}}
                  <input type="text" name="name" class="form-control m-b-5 m-t-10" required="" placeholder="Name">
                  <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control m-b-5" placeholder="Email">
                  <input type="number" name="contact" class="form-control m-b-5" placeholder="Contact" required="">
                  <input type="text" name="feedback" class="form-control m-b-5" required="" placeholder="Feedback">
                  <div class="rating">
                      Rating &nbsp;
                      <input type="radio" name="rating" id="star5" value="5"><label for="star5" title="Awesome - 5 stars"><i class="fas fa-star"></i></label>
                      <input type="radio" name="rating" id="star4" value="4"><label for="star4" title="Pretty good - 4 stars"><i class="fas fa-star"></i></label>
                      <input type="radio" name="rating" id="star3" value="3"><label for="star3" title="Meh - 3 stars"><i class="fas fa-star"></i></label>
                      <input type="radio" name="rating" id="star2" value="2"><label for="star2" title="Kinda bad - 2 stars"><i class="fas fa-star"></i></label>
                      <input type="radio" name="rating" id="star1" value="1"><label for="star1" title="Sucks big time - 1 star"><i class="fas fa-star"></i></label>
                  </div><br>
                  <input type="submit" name="submit" value="Submit" class="btn btn-success">

                  {{Form::close()}}
                </div>
                <footer class="w3-container w3-teal">
                  <h3 class="modal-h3">Thank You</h3>
                </footer>
              </div>
            </div>
          <!--modal-->
          <!--footer-->
         </main>
    </div>
<!--footer scripts-->
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>
<!--footer scripts-->
</body>
</html>
