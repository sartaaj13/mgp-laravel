@extends('admin.app')
@section('title', 'Settings')
@section('page-heading', 'Settings')
@section('settings','active')
@section('customs')
<style>

</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
@include('sweet::alert')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-warning">
					<h4>Hide/Show Feedbacks</h4>
				</div>
				<div class="card-body">
					<h4 style="float: left;">On/Off</h4>
					@if($feedback[0]==1)
						<a href="{{route('hide_feedback')}}"><button name="feedback" id="feedback" class="btn btn-info">Hide</button></a>
					@else
						<a href="{{route('show_feedback')}}"><button name="feedback" id="feedback" class="btn btn-info">Show</button></a>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-warning">
					<h4>Contact</h4>
				</div>
				<div class="card-body">
					<h5 class="text-center">If there are more then one contact numbers, Kindly seperate them with a '/'.</h5>
					{{Form::open(['route' => 'update_contact', 'method' => 'post'])}}
					<input type="text" name="contact" id="contact" value="{{$contact[0]}}" class="form-control">
					<input type="submit" name="submit" value="Update" class="btn btn-primary">
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-warning">
					<h4>Email</h4>
				</div>
				<div class="card-body">
					<h5 class="text-center">If there are more then one Emails, Kindly seperate them with a '/'.</h5>
					{{Form::open(['route' => 'update_mail', 'method' => 'post'])}}
					<input type="text" name="mail" id="mail" value="{{$mail[0]}}" class="form-control">
					<input type="submit" name="submit" value="Update" class="btn btn-primary">
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-warning">
					<h4>Address</h4>
				</div>
				<div class="card-body">
					<h5 class="text-center">Enter Your Address</h5>
					{{Form::open(['route' => 'update_address', 'method' => 'post'])}}
					<input type="text" name="address" id="address" value="{{$address[0]}}" class="form-control">
					<input type="submit" name="submit" value="Update" class="btn btn-primary">
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-warning">
					<h4>Change Banner</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="card">
						<div class="card-body setting-image">
							<img src="{{ $url[0] }}" style="height: 100%;width: 100%;">
						</div>
					</div>
					</div>
					{{Form::open(['route' => 'add_banner','method' => 'post','files' => 'true','id' => 'addbanner'])}}
					{{ csrf_field() }}
						<kbd style="float: left;">1920*800</kbd>
						<input type="file" name="file" id="file" class="form-control-file" accept="image/*">
						<br>
						<input type="submit" name="submit" value="Upload" class="btn btn-primary" style="float: left;">
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection