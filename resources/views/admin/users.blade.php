@extends('admin.app')
@section('title', 'Users')
@section('page-heading', 'User List')
@section('user','active')
@section('content')
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Visitors List</h4>
                  <p class="card-category">The Visitors who have queried or given feedback</p>
                </div>
                <div class="card-body">
	                <div class="table-responsive">
	                   <table class="table">
	                   	<thead class=" text-primary">
	                   		<th>#</th>
	                   		<th>Name</th>
	                   		<th>Email</th>
	                   		<th>Contact</th>
	                   	</thead>
	                   	<tbody>
	                   <?php $i=1; ?>
	                   		@foreach($user as $users)
	                   			<tr>
	                   				<td>{{$i}}</td>
	                   				<td>{{$users->name}}</td>
	                   				<td>{{$users->email}}</td>
	                   				<td>{{$users->contact}}</td>
	                   			</tr>
	                   			<?php $i++; ?>
	                   		@endforeach
	                   	</tbody>
	                   </table>
	                </div>
            	</div>
            </div>
		</div>
	</div>
</div>
@endsection