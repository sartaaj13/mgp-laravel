@extends('admin.app')
@section('title', 'Admin Dashboard')
@section('page-heading', 'Admin Dashboard')
@section('dashboard','active')
@section('customs')
<style>
	html,body{
		font-size: 20px;
	}
	i{
		color: #FFD700;
		float: left;
	}
	.pull-left{
		float: left;
	}
	.auto{
		margin: auto;
	}
	.pull-right{
		float: right;
	}
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
@include('sweet::alert')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header card-header-info">
					<h4>Feedbacks</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
	                   <table class="table">
	                   	<tbody class="pull-left">
	                   		<tr>
	                   			<td>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   			</td>
	                   			<td class="pull-right">
	                   				{{$five}}
	                   			</td>
	                   		</tr>
	                   		<tr>
	                   			<td>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   			</td>
	                   			<td class="pull-right">
	                   				{{$four}}
	                   			</td>
	                   		</tr>
	                   		<tr>
	                   			<td>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   			</td>
	                   			<td class="pull-right">
	                   				{{$three}}
	                   			</td>
	                   		</tr>
	                   		<tr>
	                   			<td>
	                   				<i class="fas fa-star"></i>
	                   				<i class="fas fa-star"></i>
	                   			</td>
	                   			<td class="pull-right">
	                   				{{$two}}
	                   			</td>
	                   		</tr>
	                   		<tr>
	                   			<td>
	                   				<i class="fas fa-star"></i>
	                   			</td>
	                   			<td class="pull-right">
	                   				{{$one}}
	                   			</td>
	                   		</tr>
	                   	</tbody>
	                   </table>
	               </div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-header card-header-info">
					<h4>Queries</h4>
				</div>
				<div class="card-body">
					<h4>Total Queries: {{$totalqueries}}</h4>
					<h4>Read Queries: {{$readqueries}}</h4>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-header card-header-info">
					<h4>Users</h4>
				</div>
				<div class="card-body">
					<h4>Total Users in DB: {{$user}}</h4>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-header card-header-info">
					<h4>Product</h4>
				</div>
				<div class="card-body">
					<h4>Total Products: {{$product}}</h4>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
