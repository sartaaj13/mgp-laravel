@extends('admin.app')
@section('title', 'Product Images')
@section('page-heading', 'Product Images')
@section('image','active')
@section('customs')
<style>
	.m-b-5{
		margin-bottom: 5px;
	}
	input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
    	   -webkit-appearance: none; 
          margin: 0; 
      }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
@include('sweet::alert')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-primary">
					<h4>Add Product</h4>
				</div>
				<div class="card-body">
					{{Form::open(['route' => 'add_product','method' => 'post','files' => 'true','id' => 'add_product'])}}
					<input type="text" name="item-no" id="item-no" required="" class="form-control m-b-5" placeholder="Item Number">
					<input type="number" name="price" id="price" class="form-control m-b-5" placeholder="Price">
					<input type="text" name="tag" id="tag" class="form-control m-b-5" placeholder="Tag">
					<input type="file" name="file" id="file" required="" class="form-control-file m-b-5">
					<kbd style="float: left;">File Size must be less than 5MB</kbd><br>
					<input type="submit" name="submit" id="submit" value="Insert" class="btn btn-primary" style="float: left;">  
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-primary">
					<h4>Search/Edit Product</h4>
				</div>
				<div class="card-body">
					{{Form::open(['route' => 'search_product', 'method' => 'post'])}}
					<input type="text" name="item-no" id="item-no" required="" class="form-control m-b-5" placeholder="Enter Item Number">
					<input type="submit" name="submit" id="submit" value="Search" class="btn btn-primary" style="float: left;">  
					{{Form::close()}}
				</div>
				@if(isset($searchproduct))
					<div class="row justify-content-center">
					@foreach($searchproduct as $p)
						<div class="col-md-8">
							<div class="card">
								<div class="card-body">
									<img src="{{$p->url}}" style="height: 100%; width: 100%;">
									<hr>
									{{Form::open(['route' => 'update_product','method' => 'post'])}}
									<input type="text" name="item-no" id="item-no" class="form-control" required="" value="{{$p->item_no}}" placeholder="Item No">
									<input type="text" name="price" id="price" class="form-control" required="" value="{{$p->price}}" placeholder="Price">
									<input type="text" name="tag" id="tag" class="form-control" required="" value="{{$p->tag}}" placeholder="Tag">
									<input type="hidden" name="id" value="{{$p->id}}">
									<input type="submit" name="submit" value="Update" placeholder="Update">  
									{{Form::close()}}
								</div>
								<div class="card-footer">
									<a href="{{route('deleteproduct',['id' => $p->id])}}"><button class="btn btn-danger">Delete Product</button></a>
								</div>
							</div>
						</div>	
					@endforeach
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection