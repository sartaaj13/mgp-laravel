@extends('admin.app')
@section('title', 'Owner Settings')
@section('page-heading', 'Owner Settings')
@section('owner','active')
@section('customs')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
	.w3-teal{
		background: #000!important;
	}
	.w3-modal-content{
		width: 30%!important;
	}
	@media only screen and (max-width: 768px) {
		.w3-modal-content{
			width: 90%!important;
		}
	}
</style>
@endsection
@section('content')
@include('sweet::alert')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-header card-header-primary">
					<div class="card-title">
						Add New
					</div>
				</div>
				<div class="card-body">
					{{ Form::open(['route' => 'add_owner','method' => 'post','files' => 'true']) }}
					{{ csrf_field() }}
						<input type="text" name="designation" placeholder="Designation" class="form-control" required="">
		                <input type="text" name="name" placeholder="Name" class="form-control" required="">
		                <textarea name="message" rows="1" class="form-control" required="" placeholder="Message"></textarea><br>
		                <input type="file" name="file" required="" class="form-control-file">
		                <kbd>200*200</kbd><br><br>
			            <input type="submit" name="submit" value="Upload" class="btn btn-primary btn-round">
					{{ Form::close() }}
				</div>
			</div>
		</div>
		@foreach($owner as $o)
			<div class="col-md-4">
				<div class="card card-profile">
					<div class="card-avatar">
						<img src="{{ $o->url }}" class="onhover-show">
					</div>
					<div class="card-body">
	                  	{{ Form::open(['route' => 'update_owner','method' => 'post']) }}
		                  <input type="text" name="designation" value="{{ $o->desgnation }}" placeholder="Designation" class="form-control" required="">
		                  <input type="text" name="name" value="{{ $o->name }}" placeholder="Name" class="form-control" required="">
		                  <textarea name="message" rows="7" class="form-control" required="">{{ $o->message }}</textarea>
		                  <input type="hidden" name="id" value="{{ $o->id }}">
		                  <input type="submit" name="submit" value="Update" class="btn btn-primary btn-round">
	                  	{{ Form::close() }}
	                <hr>
	                <h4 class="card-title">Update Picture</h4>
	                	{{Form::open(['route' => 'upload_ownerpic', 'method' => 'post','files' => 'true'])}}
			            {{csrf_field()}}
			            <input type="file" name="file" required="" class="form-control-file">
			            <kbd style="float: left;">200*200</kbd><br><br>
			            <input type="hidden" name="id" value="{{ $o->id }}">
			            <input type="submit" name="submit" value="Upload" class="btn btn-primary btn-round">
			            {{Form::close()}}
	                </div>
				</div>
			</div>
		@endforeach			
	</div>
</div>
@endsection