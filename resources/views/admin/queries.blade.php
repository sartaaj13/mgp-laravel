@extends('admin.app')
@section('title', 'Query Manager')
@section('page-heading', 'Query Manager')
@section('query','active')
@section('customs')
<style>
	.m-b-5{
		margin-bottom: 5px;
	}
	.pull-left{
		float: left;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card card-stats">
				<div class="card-body">
					@foreach($visitor as $v)
					@if($v->query!=null)					
						<div class="card card-stats m-b-5">
							<div class="card-header card-header-tabs card-header-success">
								<h4 class="pull-left">Name- {{$v->name}} | Email- {{$v->email}} | Contact- {{$v->contact}}</h4>
							</div>
							<div class="card-body">
								@foreach($v->query as $q)
									<div class="col-md-3 pull-left">
										<div class="card">
											<div class="card-body">
												<h4 class="pull-left">{{$q->text}}</h4>
											</div>
											<div class="card-footer">
												<a href="{{route('delete_query',['id' => $q->id])}}"><button class="btn btn-danger pull-left">Delete</button></a>
												@if($q->read_flag==0)
												<a href="{{route('update_read',['id' => $q->id])}}"><button class="btn btn-info pull-left">Mark Read</button></a>
												@else
												<button class="btn btn-success pull-left">Read</button>
												@endif
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
						<br>
					@endif
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection