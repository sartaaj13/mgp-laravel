@extends('admin.app')
@section('title', 'Feedback Manager')
@section('page-heading', 'Feedback Manager')
@section('feedback','active')
@section('customs')
<style>
	.m-b-5{
		margin-bottom: 5px;
	}
	.pull-left{
		float: left;
	}
	i{
		color: #FFD700;
	}
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		@foreach($feedback as $feed)
		<?php
			$i = 1; 
		?>
			<div class="col-md-3">
				<div class="card card-stats">
					<div class="card-header card-header-tabs card-header-info">
						<h4 class="pull-left">Name- {{$feed->name}} | Email- {{$feed->email}} | Contact- {{$feed->contact}}</h4>
					</div>
					<div class="card-body">
						<div class="pull-left">
							<span class="pull-left">
							Star Rating- 
							@for($i=1;$i<=$feed->rating;$i++)
								<i class="fas fa-star"></i>
							@endfor
							</span><br>
							<span class="pull-left">Message- {{$feed->text}}</span>
						</div>
					</div>
					<div class="card-footer">
						<a href="{{ route('delete_feedback',['id' => $feed->id]) }}"><button class="btn btn-danger pull-left">Delete</button></a>
						@if($feed->active_flag==0)
						<a href="{{ route('mark_active',['id' => $feed->id]) }}"><button class="btn btn-info pull-left">Mark Active</button></a>
						@else
						<button class="btn btn-success pull-left">Active</button>
						@endif
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
@endsection