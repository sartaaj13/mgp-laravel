<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/crown.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('images/crown.png') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    @yield('title')
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <meta name="description" content="M.G. Product Admin Panel">
  <meta name="author" content="Inderjeet Singh">
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <!-- <link href="../assets/demo/demo.css" rel="stylesheet" /> -->
  <style>
    .footer-love{
      float: none;
      color: #000;
    }
  </style>
  @yield('customs')
</head>
<body>
  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('images/sidebar-1.jpg') }}">
      <div class="logo">
        <a href="" class="simple-text logo-normal">
          M.G. Product
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item @yield('dashboard')">
            <a class="nav-link" href="{{route('admin_home')}}">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item @yield('query')">
            <a class="nav-link" href="{{route('query_page')}}">
              <i class="material-icons">query_builder</i>
              <p>Queries</p>
            </a>
          </li>
          <li class="nav-item @yield('feedback')">
            <a class="nav-link" href="{{route('feedback_page')}}">
              <i class="material-icons">feedback</i>
              <p>Feedback</p>
            </a>
          </li>
          <li class="nav-item @yield('settings')">
            <a class="nav-link" href="{{ route('show_settings') }}">
              <i class="material-icons">settings</i>
              <p>Settings</p>
            </a>
          </li>
          <li class="nav-item @yield('image')">
            <a class="nav-link" href="{{route('show_product')}}">
              <i class="material-icons">photo_library</i>
              <p>Products</p>
            </a>
          </li>
          <li class="nav-item @yield('user')">
            <a class="nav-link" href="{{route('visitors')}}">
              <i class="material-icons">supervised_user_circle</i>
              <p>Visitors</p>
            </a>
          </li>
          <li class="nav-item @yield('owner')">
            <a class="nav-link" href="{{route('owner_info')}}">
              <i class="material-icons">star</i>
              <p>Owner</p>
            </a>
          </li>
          <li class="nav-item @yield('account')">
            <a class="nav-link" href="{{route('account_setting')}}">
              <i class="material-icons">person</i>
              <p>Account Settings</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
              <i class="material-icons">lock</i>
              <p>{{ __('Logout') }}</p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
               </form>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">@yield('page-heading')</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
        </div>
      </nav>
      <div class="content">
        @yield('content')
      </div>
       <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                MG Product
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with 
            <i class="material-icons footer-love">favorite</i> by
            Inderjeet Singh for a better web.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <script src="{{ asset('js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/core/perfect-scrollbar.jquery.min.js') }}"></script>
  <script src="{{ asset('js/material-dashboard.min.js') }}" type="text/javascript"></script>
</body>
</html>