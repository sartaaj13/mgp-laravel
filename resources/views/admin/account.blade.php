@extends('admin.app')
@section('title', 'Account Settings')
@section('page-heading', 'Account Settings')
@section('account','active')
@section('customs')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('content')
@include('sweet::alert')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-info">
					Update Account Details
				</div>
				<div class="card-body">
					{{Form::open(['route' => 'update_details','method' => 'post'])}}
						<input type="text" name="name" id="name" value="{{$user->name}}" class="form-control" placeholder="Name">
						<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" id="email" value="{{$user->email}}" class="form-control" placeholder="Email">
						<input type="submit" name="submit" value="Update" class="btn btn-primary">
					{{Form::close()}}
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card card-stats">
				<div class="card-header card-header-tabs card-header-info">
					Update Password
				</div>
				<div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST" action="changePassword">
                            {{ csrf_field() }}
                            <input id="current-password" type="password" class="form-control" name="current-password" required placeholder="Current Password">
                            @if ($errors->has('current-password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('current-password') }}</strong>
                            </span>
                            @endif
                            <input id="new-password" type="password" class="form-control" name="new-password" required placeholder="New Password">

                            @if ($errors->has('new-password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('new-password') }}</strong>
                            </span>
                            @endif
                            <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required placeholder="Confirm Password">

                            <button type="submit" class="btn btn-primary">
                                Change Password
                            </button>
                        </form>
                    </div>
			</div>
		</div>
	</div>
</div>
@endsection